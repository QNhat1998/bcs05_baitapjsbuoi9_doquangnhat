class NhanVien {
    constructor(taiKhoan, hoTen, email, matKhau, ngayLam, luong, chucVu, soGio) {
        this.taiKhoan = taiKhoan
        this.hoTen = hoTen
        this.email = email
        this.matKhau = matKhau
        this.ngayLam = ngayLam
        this.luong = luong
        this.chucVu = chucVu
        this.soGio = soGio
        this.tongluong = this.chucVu === 'Giám đốc' ? this.luong * 3
            : this.chucVu === 'Trưởng phòng' ? this.luong * 2 : this.luong

        this.loaiNV = this.soGio >= 192 ? 'Xuất sắc'
            : this.soGio >= 176 ? 'Giỏi'
                : this.soGio >= 160 ? 'Khá' : 'Trung bình'

    }
}
