
class DanhSachNhanVien {
    DSNV = [
        {
            'taiKhoan': '0001',
            'hoTen': 'Đỗ Quang Nhật',
            'email': 'doquangnhat0809@gmail.com',
            'matKhau': '080998',
            'ngayLam': '10/19/2022',
            'luong': 10000000,
            'chucVu': 'Trưởng phòng',
            'soGio': '176',
            'tongluong': 30000000,
            'loaiNV': 'Giỏi'
        },
        {
            'taiKhoan': '0002',
            'hoTen': 'Võ Mỹ Hòa',
            'email': 'vomyhoa2k@gmail.com',
            'matKhau': '080998',
            'ngayLam': '10/19/2022',
            'luong': 1000000,
            'chucVu': 'Nhân viên',
            'soGio': '80',
            'tongluong': 1000000,
            'loaiNV': 'Trung Bình'
        },
        {
            'taiKhoan': '0003',
            'hoTen': 'Đỗ Quang Khải',
            'email': 'quangkhai0901@gmail.com',
            'matKhau': '080998',
            'ngayLam': '10/10/2022',
            'luong': 20000000,
            'chucVu': 'Giám đốc',
            'soGio': '200',
            'tongluong': 60000000,
            'loaiNV': 'Xuất sắc'
        },
        {
            'taiKhoan': '0004',
            'hoTen': 'Đặng Thị Thảo Vy',
            'email': 'thaovy2k@gmail.com',
            'matKhau': '080998',
            'ngayLam': '10/10/2022',
            'luong': 1000000,
            'chucVu': 'Nhân viên',
            'soGio': '160',
            'tongluong': 1000000,
            'loaiNV': 'Khá'
        }
    ]

    themNhanVien = (nhanVien) => {
        this.DSNV.push(nhanVien);
    }

    xoaNhanVien = (index) => {
        if (index == -1)
            return false;
        this.DSNV.splice(index, 1)
        return true
    }

    capNhatNhanVien = (nvCapNhap) => {
        let index = this.DSNV.findIndex(nv => nv.taiKhoan == nvCapNhap.taiKhoan)
        if(index != -1){
            this.DSNV[index].hoTen = nvCapNhap.hoTen
            this.DSNV[index].email = nvCapNhap.email
            this.DSNV[index].matKhau = nvCapNhap.matKhau
            this.DSNV[index].ngayLam = nvCapNhap.ngayLam
            this.DSNV[index].luong = nvCapNhap.luong
            this.DSNV[index].chucVu = nvCapNhap.chucVu
            this.DSNV[index].soGio = nvCapNhap.soGio
            return true;
        }
        return false;
    }
    kiemTraTonTai = (taiKhoan) => this.DSNV.findIndex(nv => nv.taiKhoan == taiKhoan) !== -1 ? true : false
    timNV = (loaiNV) => {

    }
}

