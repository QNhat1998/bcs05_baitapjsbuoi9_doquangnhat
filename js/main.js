const val = new Validation()
let dsNhanVien = new DanhSachNhanVien()
const getEle = id => document.getElementById(id)
checkValidation = () => {
    let taiKhoan = getEle('tknv').value,
        hoTen = getEle('name').value,
        matKhau = getEle('password').value,
        email = getEle('email').value,
        ngayLam = getEle('datepicker').value,
        luong = getEle('luongCB').value,
        chucVu = getEle('chucvu').value,
        gioLam = getEle('gioLam').value
        console.log(taiKhoan,hoTen,matKhau,email,ngayLam,luong,chucVu,gioLam)
    let valid = 0
    if (val.kiemTraRong(taiKhoan, 'tbTKNV') && valid++)
        if (val.kiemTraSo(taiKhoan, 'tbTKNV') && valid++)
            val.kiemTraDoDai(taiKhoan, 'tbTKNV', 4, 6) && valid++
    if (val.kiemTraRong(hoTen, 'tbTen') && valid++)
        val.kiemTraChu(hoTen, 'tbTen') && valid++
    if (val.kiemTraRong(matKhau, 'tbMatKhau') && valid++)
        val.kiemTraDoDai(matKhau, 'tbMatKhau', 6, 10) && valid++
    if (val.kiemTraRong(email, 'tbEmail') && valid++)
        val.kiemTraEmail(email, 'tbEmail') && valid++

        if (val.kiemTraSo(luong, 'tbLuongCB') && valid++)
            val.kiemTraGiaTri(luong, 'tbLuongCB', 1e6, 2e7) && valid++
    val.kiemTraRong(chucVu, 'tbChucVu') && valid++
    val.kiemTraRong(ngayLam, 'tbNgay') && valid++
    if (val.kiemTraRong(gioLam, 'tbGiolam') && valid++)
        if (val.kiemTraSo(gioLam, 'tbGiolam') && valid++)
            val.kiemTraGiaTri(gioLam, 'tbGiolam', 80, 200) && valid++
    return valid
}
hienThongTinNhanVien = (dsnv) => {
    let strNhanVien = ''
    if (dsNhanVien.DSNV.length != 0) {
        dsnv.map((nv, index) => {
            strNhanVien += `
                <tr>
                    <td>${nv.taiKhoan}</td>
                    <td>${nv.hoTen}</td>
                    <td>${nv.email}</td>
                    <td>${nv.ngayLam.toLocaleString('en-GB')}</td>
                    <td>${nv.chucVu}</td>
                    <td>${nv.tongluong.toLocaleString()}</td>
                    <td>${nv.loaiNV}</td>
                    <td><button class='btn btn-danger' onclick='xoaNhanVien("${index}")' />Xóa</td>
                    <td><button data-toggle="modal" data-target="#myModal"  class='btn btn-success' onclick='hienNhanVienModal(${index});trangThai(true)'/>Sửa</td>
                </tr>
            `
        })
    } else {
        strNhanVien = `
        <tr height='200px'>
            <td colspan='8' style='vertical-align:middle;'>
            <h1 class='text-danger text-center'>Dữ liệu trống /ᐠ. ｡.ᐟ\ᵐᵉᵒʷˎˊ˗</h1>
            </td>
        </tr>
    `
    }
    getEle('tableDanhSach').innerHTML = strNhanVien

}
themNhanVien = () => {
    let tbTrangThai = getEle('tbTrangThai')
    if (checkValidation() == 15) {
        let taiKhoan = getEle('tknv').value
        if (!dsNhanVien.kiemTraTonTai(taiKhoan)) {
            let hoTen = getEle('name').value,
                matKhau = getEle('password').value,
                email = getEle('email').value,
                ngayLam = getEle('datepicker').value,
                luong = getEle('luongCB').value,
                chucVu = getEle('chucvu').value,
                gioLam = getEle('gioLam').value
            let nhanVien = new NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luong, chucVu, gioLam)
            dsNhanVien.themNhanVien(nhanVien)
            hienThongTinNhanVien(dsNhanVien.DSNV)
            setStorage()
            tbTrangThai.innerHTML = `Thêm kẻ tội đồ ${hoTen} thành công (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧`
        } else {
            alert('Tài khoản tồn tại')
            getEle('tknv').focus()
            getEle('tknv').value = ''
            return
        }

    }
}
xoaNhanVien = (index) => {
    let tbTrangThai = getEle('tbTrangThai')
    let hoTen = dsNhanVien.DSNV[index].hoTen
    dsNhanVien.xoaNhanVien(index) == true ? tbTrangThai.innerHTML = `Xóa kẻ xấu ${hoTen}  thành công o((>ω< ))o`
        : alert('Kẻ xấu này không tìm thấy')
    hienThongTinNhanVien(dsNhanVien.DSNV)
    setStorage()
}
hienNhanVienModal = (index) => {
    getEle('tknv').value = dsNhanVien.DSNV[index].taiKhoan
    getEle('name').value = dsNhanVien.DSNV[index].hoTen
    getEle('password').value = dsNhanVien.DSNV[index].matKhau
    getEle('email').value = dsNhanVien.DSNV[index].email
    getEle('datepicker').value = dsNhanVien.DSNV[index].ngayLam
    getEle('luongCB').value = dsNhanVien.DSNV[index].luong
    getEle('chucvu').value = dsNhanVien.DSNV[index].chucVu
    getEle('gioLam').value = dsNhanVien.DSNV[index].soGio

    suaNhanVien = () => {
        console.log(checkValidation())
        if(checkValidation() == 15){
            let taiKhoan = getEle('tknv').value,
            hoTen = getEle('name').value,
            matKhau = getEle('password').value,
            email = getEle('email').value,
            ngayLam = getEle('datepicker').value,
            luong = getEle('luongCB').value,
            chucVu = getEle('chucvu').value,
            gioLam = getEle('gioLam').value
        let nvUpdate = new NhanVien(taiKhoan, hoTen, email, matKhau, ngayLam, luong, chucVu, gioLam)
        dsNhanVien.capNhatNhanVien(nvUpdate) == true ? tbTrangThai.innerHTML = `Sửa kẻ xấu ${nvUpdate.hoTen}  thành công (►__◄)`
        : alert('Kẻ xấu này không tìm thấy')
        hienThongTinNhanVien(dsNhanVien.DSNV)
        setStorage()
        }
    }

}
setStorage = () => {
    let jsonDanhSachNhanVien = JSON.stringify(dsNhanVien.DSNV)
    localStorage.setItem("DanhSachNhanVien", jsonDanhSachNhanVien)
}

setStorage()
getStorage = () => {
    let jsonDanhSachNhanVien = localStorage.getItem("DanhSachNhanVien")
    if (jsonDanhSachNhanVien) {
        var mangNhanVien = JSON.parse(jsonDanhSachNhanVien);
        dsNhanVien.DSNV = mangNhanVien;
        hienThongTinNhanVien(dsNhanVien.DSNV)
    }
}
getStorage();
trangThai = (b) => {
    getEle('tknv').disabled = b
    getEle('btnThemNV').disabled = b
    getEle('btnCapNhat').disabled = !b
}